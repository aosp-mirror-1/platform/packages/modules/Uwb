// Copyright (C) 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_team: "trendy_team_fwk_uwb",
    default_applicable_licenses: ["Android-Apache-2.0"],
}

java_library {
    name: "ranging_uwb_backend",
    sdk_version: "system_UpsideDownCake",
    min_sdk_version: "33",
    installable: false,
    srcs: [
        "src/**/*.java",
    ],
    static_libs: [
        "androidx.annotation_annotation",
        "androidx.concurrent_concurrent-futures",
        "com.uwb.support.fira",
        "com.uwb.support.multichip",
        "com.uwb.support.dltdoa",
        "guava",
    ],
    apex_available: [
        "com.android.uwb",
        "com.android.tethering",
        "//apex_available:platform",
    ],
    visibility: [
        ":__subpackages__",
        "//packages/modules/Uwb/ranging:__subpackages__",
        "//packages/modules/Connectivity/remoteauth:__subpackages__",
    ],
}
